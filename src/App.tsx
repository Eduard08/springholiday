import React, { useState } from 'react';
import './App.scss';
import ImageMap from './Components/ImageMap/ImageMap';
import QuestForm from './Components/QuestForm/QuestForm';
import NavSteps from './Components/Steps/NavSteps';
import questData from './questData';

function App() {
  const [step, setStep] = useState(0);
  const markerCoords = { top: 75, left: 8 }

  const initialStatistic = {
    1: false,
    2: false,
    3: false,
    4: false,
    5: false,
    6: false,
    7: false,
    8: false,
    9: false,
    10: false,
  };
  const [statistic, updateStatistic] = useState<{[key: number]: boolean}>(initialStatistic);

  const checkAnswer = (data: {askId: number, result: boolean}) => {
    const updateData = {[data.askId]: data.result}
    updateStatistic({...statistic, ...updateData});
    setStep(Math.min(questData.length - 1, step + 1));
  }

  return (
    <div className="App">
      <header className="App-header">
        К любому вопросу можно вернуться и ответить снова!
      </header>
      <main>
        <NavSteps currentStep={step} setCurrentStep={setStep} statistic={statistic}/>
        <QuestForm handler={checkAnswer} question={questData[step]}/>
        <ImageMap statistic={statistic} coords={markerCoords}/>
      </main>
    </div>
  );
}

export default App;

