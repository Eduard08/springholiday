import React from 'react';
import { Steps } from 'antd';
import './NavSteps.scss';

const { Step } = Steps;

type NavStepsProps = {
    currentStep: number, 
    setCurrentStep: (value: number) => void,
    statistic: {[key: number]: boolean},
}
const NavSteps: React.FC<NavStepsProps> = ({
    currentStep, setCurrentStep, statistic
}: NavStepsProps) => {
    
    return (
        <div className="nav-steps">
            <Steps size='small' current={currentStep} onChange={(value) => setCurrentStep(value)}>
                {Object.values(statistic).map((step, i) => {
                    const status = currentStep > i ? (step === false ? 'error' : 'finish') : (step === true ? 'finish' : 'wait');
                    return <Step 
                                status={status}
                                className={currentStep === i ? 'step-current' : ''}
                            />
                })}
            </Steps>
        </div>
    )
}
// statuses: process wait error finish  
export default NavSteps;