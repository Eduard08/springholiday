import React, { useEffect, useState } from 'react';
import './ImageMap.scss';

type ImageMapProps = {
    statistic: {[key: number]: boolean}, 
    coords: { top: number, left: number }
}
const ImageMap: React.FC<ImageMapProps> = ({ statistic, coords }: ImageMapProps) => {
    const [showCoords, setShowCoords] = useState(false);

    useEffect(() => {
        const notComplete = Object.values(statistic).some(item => item === false);
        if (!notComplete) {
            setShowCoords(true);
        } else setShowCoords(false);
        
    }, [statistic]);

    const markerStyleProps = {
        background: 'url(images/marker-icon.webp) no-repeat center',
        backgroundSize: 'cover',
        top: `${coords.top}%`,
        left: `${coords.left}%`,
    };

    return (
        <div className="image-map">
            {showCoords 
            ? <span className='image-map-marker' style={markerStyleProps} /> 
            : null}    

            {Object.values(statistic).map((answer, i) => {
                const path = answer === true ? 'truthy' : 'falsy';

                if (i + 1 < 10) {
                    return (
                        <div className="image-wrapper" key={i.toString()}>
                            <img src={`images/${path}/${i + 1}.jpg`} />
                        </div>
                    )
                }
                
            })}
        </div>
    )
}

export default ImageMap;