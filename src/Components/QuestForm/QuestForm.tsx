import React from 'react';
import { Button, Form, Input, Radio } from 'antd';
import './QuestForm.scss';

const FormItem = Form.Item;
const QuestForm: React.FC<QuestFormProps> = ({ question, handler }: QuestFormProps) => {
    const itemLayout = { offset: 8, span: 8 };

    const handleFinish = (e: {[key: string]: string}) => {
        const data = {
            askId: question.askId,
            result: e.answer === question.trueAnswer,
        };
        handler(data);
    };

    return (
      <div className="quest-form-wrapper">
        <div className="quest__question">{question.ask}</div>
        <Form onFinish={handleFinish}>
            <FormItem wrapperCol={itemLayout} name={'answer'}>
                <Radio.Group options={question.answers} />
            </FormItem>
            <FormItem wrapperCol={itemLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </FormItem>
        </Form>
      </div>
    )
  }
  
  type QuestFormProps = {
    question: {
        askId: number,
        ask: string,
        answers: { label: string, value: string }[],
        trueAnswer: string
    },
    handler: (e: any) => void
  }

  export default QuestForm;